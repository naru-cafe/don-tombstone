FROM nginx:1.25.3-alpine

RUN rm /etc/nginx/conf.d/default.conf
COPY nginx.conf /etc/nginx/conf.d

COPY 410.html /usr/share/nginx/html/410.html

# nginx 시작
CMD ["nginx", "-g", "daemon off;"]